﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumpersScore : MonoBehaviour
{

    public int bumperValue = 50;

    // Use this for initialization
    void OnCollisionEnter()
    {

        ScoreManager.score += bumperValue;

    }
}
