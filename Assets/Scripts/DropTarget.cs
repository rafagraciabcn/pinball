﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTarget : MonoBehaviour
{

    public static List<DropTarget> dropTargets = new List<DropTarget>();

    public float dropDistance = 1.0f;

    public int bankID = 0;

    public float resetDeloy = 0.5f;

    public int targetValue = 50;

    public bool isDropped = false;

    public AudioClip[] audioClip;

    // Use this for initialization
    void Start()
    {
        dropTargets.Add(this);
    }
    

    void OnCollisionEnter()

    {

        if (!isDropped)

        {

            PlaySound(0);

            transform.position += Vector3.down * dropDistance;

            isDropped = true;

            ScoreManager.score += targetValue;

            bool resetBank = true;

            foreach (DropTarget target in dropTargets)
            {
                if (target.bankID == bankID)
                {

                    if (!target.isDropped)

                    {

                        resetBank = false;

                    }
                }
            }

            if (resetBank)

            {

                ScoreManager.score = ScoreManager.score * 3;

                Invoke("ResetBank", resetDeloy);

            }
        }
    }

    void PlaySound(int clip)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

    }


    public void ResetBank()
    {
        foreach (DropTarget target in dropTargets)
        {

            if (target.bankID == bankID)

            {

                target.transform.position += Vector3.up * dropDistance;

                target.isDropped = false;

            }
        }
    }
}