﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsertCoin : MonoBehaviour
{

    public Transform Ball;
    public Transform RespawnPoint;
    public Transform DeadPoint;
    public static int life = 3;
    public AudioClip[] audioClip;

    void OnMouseDown()
    {

        PlaySound(0);
        life = 3;
        Debug.LogError("You have: " + life + " lifes");
        Ball.transform.position = RespawnPoint.transform.position;
        ScoreManager.score = 0;
        DropTarget.ResetBank();

    }
    void PlaySound(int clip)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

    }


}