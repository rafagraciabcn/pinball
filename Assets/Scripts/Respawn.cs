﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {
    
    public Transform Ball;
    public Transform RespawnPoint;
    public Transform DeadPoint;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ball")

            if(InsertCoin.life > 0)
        {
            InsertCoin.life = InsertCoin.life -1;
            
            Debug.LogError("You have: " + InsertCoin.life + " lifes");
            
            Ball.transform.position = RespawnPoint.transform.position;

        }
         if(InsertCoin.life <= 0)
            {

                Ball.transform.position = DeadPoint.transform.position;
                Debug.LogError("GAME OVER");

            }
    }

}
