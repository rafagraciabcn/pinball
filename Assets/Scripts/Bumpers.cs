﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumpers : MonoBehaviour
{

    public float power = 120.0f;
    public float radio = 1.0f;
    public AudioClip[] audioClip;

    // Use this for initialization
    void OnCollisionEnter()
    {

        foreach (Collider col in Physics.OverlapSphere(transform.position, radio))

        {
            if (col.GetComponent<Rigidbody>())

            {

                col.attachedRigidbody.AddExplosionForce(power, transform.position, radio);

                PlaySound(0);

            }
        }
    }

    void PlaySound(int clip)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

    }

}
	
