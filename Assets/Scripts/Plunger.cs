﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plunger : MonoBehaviour {

string inputButtonName = "Pull";
float distance = 2;
float speed = 1;
float power = 1500;
public GameObject ball;
public Rigidbody rb;

public bool ready = false;
public bool fire = false;
public float moveCount = 0;
private Vector3 origen;
public AudioClip[] audioClip;

    void Start()
    {

        origen = transform.position;

    }
    /*
    void OnColEnter(Collider other)
        {
            rb = ball.GetComponent<Rigidbody>();

            if (other.tag == "Ball")

            {
                ready = true;
            }
        }
        */

    void Update()
    {
      
        if (Input.GetButton(inputButtonName))
        {
            if (moveCount < distance)
            {
                transform.Translate(0, -(-speed * Time.deltaTime), 0);
                moveCount += speed * Time.deltaTime;
                fire = true;
            }
        }
        else
            if (moveCount > 0)
            {
                if (fire && ready)
                {
                    ball.transform.TransformDirection(Vector3.forward * 10);
                    rb.AddForce(0, 0, moveCount * power);
                    fire = false;
                    ready = false;

            }
            PlaySound(0);
            transform.Translate(0, -20 * Time.deltaTime, 0);
                moveCount -= 20 * Time.deltaTime;
            }

            if (moveCount >= 1)
        {

            fire = false;
            moveCount = 1;
            transform.Translate(0, 0, 0);
            speed = 0;

            
        }


            if (moveCount <= 0)
            {
                fire = false;
                moveCount = 0;
                transform.position = origen;
            speed = 1;
        }

        }
    void PlaySound(int clip)
    {

        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();

    }

}