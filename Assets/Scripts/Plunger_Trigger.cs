﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Plunger_Trigger: MonoBehaviour
{
    float minPower;
    float power;
    public float maxPower = 1000f;
    List<Rigidbody> ballList;
    public Slider powerSlider;
    bool ballReady;


    void Start(){

        powerSlider.maxValue = maxPower;
        powerSlider.minValue = 0f;
        ballList = new List<Rigidbody>();
        
    }
    void Update(){

        if (ballReady)

        {
        
            powerSlider.gameObject.SetActive(true);

        }else

        {

            powerSlider.gameObject.SetActive(false);

        }

        powerSlider.value = power;

        if (ballList.Count > 0)
        {
        
            ballReady = true;

            if (Input.GetKey(KeyCode.Space))
            {

                if (power <= maxPower)
                {

                    power += 100 * Time.deltaTime;

                }

            }

            if (Input.GetKeyUp(KeyCode.Space))
            {

                foreach (Rigidbody r in ballList)

                {

                    r.AddForce(power * Vector3.forward);

                }

            }

        }else

        {

            ballReady = false;

            power = 0f;

        }

    }

    private void OnTriggerEnter(Collider other){

        if (other.tag == "Ball")
        {

            ballList.Add(other.gameObject.GetComponent<Rigidbody>());

        }

    }

    private void OnTriggerExit(Collider other){

        if (other.tag == "Ball")

        {

            ballList.Remove(other.gameObject.GetComponent<Rigidbody>());

            power = 0f;

        }

    }

}